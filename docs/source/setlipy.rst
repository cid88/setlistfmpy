setlipy package
===============

Submodules
----------

setlipy.auth module
-------------------

.. automodule:: setlipy.auth
   :members:
   :undoc-members:
   :show-inheritance:

setlipy.client module
---------------------

.. automodule:: setlipy.client
   :members:
   :undoc-members:
   :show-inheritance:

setlipy.string\_encoding module
-------------------------------

.. automodule:: setlipy.string_encoding
   :members:
   :undoc-members:
   :show-inheritance:

setlipy.test\_client module
---------------------------

.. automodule:: setlipy.test_client
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: setlipy
   :members:
   :undoc-members:
   :show-inheritance:
