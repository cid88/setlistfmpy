.. image:: images/banner.jpg
   :width: 100 %

Welcome to Setlipy's documentation!
===================================
*Setlipy* is a lightweight and easy to use Python library for the `Setlist.fm Web API <https://api.setlist.fm/docs/1.0/index.html>`.


Features
========
*Setlipy* supports all end points to the Setlist.fm Web API. For more details see `Setlist.fm Web API page <https://api.setlist.fm/docs/1.0/index.html>`.


Installation
============

Install with::

   pip install setlipy

Or you can check out the source code here: <https://gitlab.com/cid88/setlistfmpy>


Getting Started
===============
To get started, install setlipy, register a free account on https://www.setlist.fm/signup and apply for an API key on
https://www.setlist.fm/settings/api
Add your API Key to your environment.

Quick start
-----------

Get all setlists from an artist and a specific year.
Default as JSON file format.::

   from setlipy import client

   sfm = client.Setlipy(auth="YOUR_API_KEY")

   results = sfm.setlists(artist_name="The Rolling Stones", year="2022")

   json_dump = (results.json())

   for idx, setlists in enumerate(json_dump["setlist"]):
       print(idx, setlists)


Default file format is JSON. Use the following to request the data as XML::

   # Get all setlists from an artist and a specific year.
   # As XML file format.

   import xml
   import xml.etree.ElementTree as ET

   from setlipy import client

   sfm = client.Setlipy(file_format="xml", auth="YOUR_API_KEY")

   results = sfm.setlists(artist_name="The Rolling Stones", year="2022")

   string_xml = results.content
   tree = xml.etree.ElementTree.fromstring(results.content)
   print(xml.etree.ElementTree.dump(tree))

