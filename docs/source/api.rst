API Reference
=============

.. toctree::
   :maxdepth: 4

.. automodule:: setlipy.client
    :members:
    :undoc-members:
    :special-members: __init__
    :show-inheritance:

.. automodule:: setlipy.string_encoding
    :members:
    :undoc-members:
    :special-members: __init__
    :show-inheritance: