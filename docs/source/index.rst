.. Setlipy documentation master file, created by
   sphinx-quickstart on Fri Jul 29 22:02:27 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Setlipy's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   welcome
   api


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

