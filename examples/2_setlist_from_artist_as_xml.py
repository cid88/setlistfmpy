# Get all setlists from an artist and a specific year.
# As XML file format.
import xml
import xml.etree.ElementTree as ET

from setlipy import client
from setlipy import auth

sfm = client.Setlipy(file_format="xml", auth=auth.auth)

results = sfm.setlists(artist_name="The Rolling Stones", year="2022")

string_xml = results.content
tree = xml.etree.ElementTree.fromstring(results.content)
print(xml.etree.ElementTree.dump(tree))
