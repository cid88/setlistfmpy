# Get all setlists from an artist and a specific year.
# Default as JSON file format.
from setlipy import client
from setlipy import auth

sfm = client.Setlipy(auth=auth.auth)

results = sfm.setlists(artist_name="The Rolling Stones", year="2022")

json_dump = (results.json())

for idx, setlists in enumerate(json_dump["setlist"]):
    print(idx, setlists)
