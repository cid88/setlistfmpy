from setlipy.auth import auth
from setlipy.client import Setlipy

sfm = Setlipy(auth=auth)


def test_setlists_json_data1():
    results = sfm.setlists(artist_name="The Rolling Stones")

    json_dump = results.json()
    artist_name = json_dump["setlist"][0]["artist"]["name"]

    assert artist_name == "The Rolling Stones"


def test_setlists_json_data2():
    results = sfm.setlists(artist_name="The Rolling Stones", year="2022")

    json_dump = results.json()
    artist_name = json_dump["setlist"][0]["artist"]["name"]
    event_date = json_dump["setlist"][0]["eventDate"]
    # eventDate = "15-07-2022"
    event_year = event_date.split("-")[2]

    assert artist_name == "The Rolling Stones" and event_year == "2022"


def test_setlists_json_data3():
    results = sfm.setlists(
        artist_name="The Rolling Stones", year="2022", city_name="Munich"
    )

    json_dump = results.json()
    artist_name = json_dump["setlist"][0]["artist"]["name"]
    event_date = json_dump["setlist"][0]["eventDate"]
    # eventDate = "15-07-2022"
    event_year = event_date.split("-")[2]
    city_name = json_dump["setlist"][0]["venue"]["city"]["name"]

    assert (
        artist_name == "The Rolling Stones"
        and event_year == "2022"
        and city_name == "Munich"
    )


def test_setlists_json_data4():
    results = sfm.setlists(
        artist_name="The Rolling Stones", year="2022", city_name="Munich"
    )

    json_dump = results.json()
    artist_name = json_dump["setlist"][0]["artist"]["name"]
    event_date = json_dump["setlist"][0]["eventDate"]
    # eventDate = "15-07-2022"
    event_year = event_date.split("-")[2]
    city_name = json_dump["setlist"][0]["venue"]["city"]["name"]

    assert (
        artist_name == "The Rolling Stones"
        and event_year == "2022"
        and city_name == "Munich"
    )


def test_setlists_json_data5():
    results = sfm.setlists(artist_name="The Rolling Stones", date_of_event="05-06-2022")

    json_dump = results.json()
    artist_name = json_dump["setlist"][0]["artist"]["name"]
    event_date = json_dump["setlist"][0]["eventDate"]

    assert artist_name == "The Rolling Stones" and event_date == "05-06-2022"


def test_setlists_json_data6():
    results = sfm.setlists(
        artist_name="The Beatles",
        year="1960",
        city_name="Liverpool",
        last_updated="2013-10-18T21:12:14.000+0000",
    )

    json_dump = results.json()
    artist_name = json_dump["setlist"][0]["artist"]["name"]
    event_date = json_dump["setlist"][0]["eventDate"]
    # eventDate = "15-07-2022"
    event_year = event_date.split("-")[2]
    city_name = json_dump["setlist"][0]["venue"]["city"]["name"]
    last_updated = json_dump["setlist"][0]["lastUpdated"]

    assert (
        artist_name == "The Beatles"
        and event_year == "1960"
        and city_name == "Liverpool"
        and last_updated == "2013-10-18T21:12:14.000+0000"
    )


def test_setlists_for_artist_with_special_char1():
    results = sfm.setlists(artist_name="Dead & Company", year="2022")

    json_dump = results.json()
    artist_name = json_dump["setlist"][0]["artist"]["name"]

    assert artist_name == "Dead & Company"


def test_setlists_for_artist_with_special_char2():
    results = sfm.setlists(artist_name="Against Me!", year="2020")

    json_dump = results.json()
    artist_name = json_dump["setlist"][0]["artist"]["name"]

    assert artist_name == "Against Me!"
