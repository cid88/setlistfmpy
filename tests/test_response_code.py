from setlipy.client import Setlipy
from setlipy.auth import auth

sfm = Setlipy(auth=auth)


def test_artists():
    response = sfm.artists(artist_name="Any Given Day")

    assert response.status_code == 200


def test_setlists():
    response = sfm.setlists(artist_name="Bury Tomorrow", year="2022")

    assert response.status_code == 200


def test_cities():
    response = sfm.cities(name="Frankfurt")

    assert response.status_code == 200


def test_countries():
    response = sfm.countries()

    assert response.status_code == 200


def test_venues():
    response = sfm.venues(name="Batschkapp")

    assert response.status_code == 200


def test_setlist_version():
    response = sfm.setlist_version(version_id="g33f4c09d")

    assert response.status_code == 200


def test_setlist_by_id():
    response = sfm.setlist_by_id(setlist_id="43b4374f")

    assert response.status_code == 200


def test_user():
    response = sfm.user(user_id="drewcadaver")

    assert response.status_code == 200


def test_user_attended():
    response = sfm.user_attended(user_id="drewcadaver")

    assert response.status_code == 200


def test_user_edited():
    response = sfm.user_edited(user_id="drewcadaver")

    assert response.status_code == 200


def test_venue():
    response = sfm.venue(venue_id="6bd6ca6e")

    assert response.status_code == 200


def test_setlists_by_venue():
    response = sfm.setlists_for_venue(venue_id="6bd6ca6e")

    assert response.status_code == 200


# def test_city():
#     response = sfm.city(geo_id="")
#
#     assert response.status_code == 200


def test_artist_for_musicbrainz_id():
    response = sfm.artist_for_musicbrainz_id(
        mbid="b10bbbfc-cf9e-42e0-be17-e2c3e1d2600d"
    )

    assert response.status_code == 200


def test_artist_setlists_for_musicbrainz_id():
    response = sfm.artist_for_musicbrainz_id(
        mbid="b10bbbfc-cf9e-42e0-be17-e2c3e1d2600d"
    )

    assert response.status_code == 200
